# Ubuntu OpenJFX8 build

Build of OpenJFX 8 for Ubuntu platforms. There are currently no openJFX packages for Ubuntu that are compatible with JDK 8, so this project builds it from source so that it can be copied onto Ubuntu platforms